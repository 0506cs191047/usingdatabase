﻿using CrudOperationUsingDatabase.Models;
using CrudOperationUsingDatabase.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CrudOperationUsingDatabase.Controllers
{
    public class UserController : Controller
    {
        // Create Constructor
        // Call service Layer using IUserService
        // don't Use new keyword
        // Start using Constructor DI(dependency Injection)
        readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        // Action - Index-List<User> GetAllUsers
        public IActionResult Index()
        {
            List<User> userList = _userService.GetAllUsers();
            return View(userList);
        }
        // Action - AddUser
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(User user)
        {
            bool userAddStatus = _userService.AddUser(user);
            return RedirectToAction("Index");
        }
    }
}
