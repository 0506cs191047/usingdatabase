﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CrudOperationUsingDatabase.Models
{
    public class User
    {
        // [Key,DatabaseGenerated(DatabaseGeneratedOption.None)] 
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)] // In the place of Identity option we don't written the code of USerRepository for increment the Id it automatically generated.
        public int Id { get; set; }
        public string Name { get; set; }
        public string IsRegular { get; set; }
        public string City { get; set; }

    }
}
