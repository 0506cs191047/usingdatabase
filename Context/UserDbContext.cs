﻿using CrudOperationUsingDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudOperationUsingDatabase.Context
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext>options):base(options) 
        { 
            Database.EnsureCreated();
        }
        //Tables
        public DbSet<User> Users { get; set; }

    }
}
