﻿using CrudOperationUsingDatabase.Models;
using System.Collections.Generic;

namespace CrudOperationUsingDatabase.Service
{
    public interface IUserService
    {
        bool AddUser(User user);
        List<User> GetAllUsers();
    }
}
