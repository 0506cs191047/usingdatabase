﻿using CrudOperationUsingDatabase.Models;
using CrudOperationUsingDatabase.Repository;
using System.Collections.Generic;

namespace CrudOperationUsingDatabase.Service
{
    public class UserService : IUserService
    {
        // Call Repository Layer of IUserRepository
        // Don't Create a new Keyword
        // Call Constructor and use Constructor DI
        readonly IUserRepository userRepository;
        public UserService(IUserRepository _userRepository)
        {
            userRepository = _userRepository;
        }

        public bool AddUser(User user)
        {
            User exists = userRepository.GetUserByName(user.Name);
            if (exists == null)
            {
                userRepository.AddUser(user);
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<User> GetAllUsers()
        {
            List<User> users = userRepository.GetAllUsers();
            return users;
        }
    }
}
