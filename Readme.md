﻿# ASP.Net Core MVC with Layered Approach with List and Introduction to Context :----

# UserApp

# Features 
    * CRUD
        * C ----- Create
        * R ----- Read(Get)
        * U ----- Update(Edit)
        * D ----- Delete

# Tech Stacks
    * C#
    * ASP.Net Core MVC
    * LINQ

# Code Standard
    * Layered Approach
        Controller ----> Service ----> Repository ----> Context ----> SQL Server

    * Seperation of Concern 


# DataBase Connection :----
   * ConText ----> Its depends on table data.
   * Then you going to the nuGEt package and you install the microsoft entityframework.
   * Then you used namespace into the context layer.
   * Then inherited the context class using inbuilt class of DbContext.
   * Then create the method for the table making use :-- public DbSet<User> Users { get; set; }
   * Now you going to the userRepository and create a instance of context layer and with the help of this instance you changed the functionality of the function.
   * These are some functionality:----
       // var usersList = _userDbContext.Users.AsQueryable();
       // _userDbContext.Users.Add(user);
       // _userDbContext.SaveChanges();
       // _userDbContext.Users.ToList();
       // _userDbContext.Users.Where(u => u.Name == name).FirstOrDefault();


# Using Entity Framework :-- 
    * We are going to our appsettings and written after the line number 8
        },
             "ConnectionStrings": {
             "UserSQLConnectionStr": "Server=;Database=;Trusted_Connection=;"
        },
        "AllowedHosts":  "*"

   * Then we Create a Constructor in to the context Layer and choose the :-- 
       Database.EnsureCreated(); // It will create a Database (It check the DataBase is created or not.)
       and we enter the inbuild class of (DbContextOptions<UserDbContext>options) into the constructor and inherited by the Base(options).

   * Then we are going to startup.cs file and set the connection between the many option.

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddScoped<IUserService,UserService>(); // AddScoped,AddSingleton- DI
            //services.AddSingleton<IUserRepository,UserRepository>(); // Only One Time Instance Create (DI) only use for List option.
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddDbContext<UserDbContext>(u=>u.UseSqlServer(userSQLConnection)); // It is a Inheriting class inherited by UserDbContext.
        }
   * We are going to NuGet packet and install the Microsoft.entityFrameworkCore.sqlserver(Jis version me aap kaam krte hai vahi download kare)
   * Then we Add a string conncetion :-- string userSQLConnection = Configuration.GetConnectionString("UserSQLConnectionStr");
   * Then it may not give me the error bt Id and Id you make as a primary key, then you used :--  [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
# Always updated the database name into the appsettings otherwise it give us the error.

* Database.EnsureCreated() ----> Its not a permanent option to go for the create the database. It is not added the new value into
                                 the database. Its not good approach to create a database.

* Here we go for the second way to create the Database are as follow :--
    * First you go for the NuGet packet and choose the :----> Microsoft.EntityFrameworkCore.Tools(This package is only used for
                                                               to change and update the Schema(Database)).
    * Then go for the tools and for the NuGet packet Manager and go for the Package Manager Console and written the some steps :----
        These three command for the EntityFrameworkCore migration command for updating a database :---> 

            // enable-migrations 
            // add-migration "codefirst approach" 
            // update-database 


Assignment :-- 1- Using list create crud operation(dotnet 5)
               2- Using database create crud operation




