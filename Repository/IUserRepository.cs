﻿using CrudOperationUsingDatabase.Models;
using System.Collections.Generic;

namespace CrudOperationUsingDatabase.Repository
{
    public interface IUserRepository
    {
        void AddUser(User user);
        List<User> GetAllUsers();
        User GetUserByName(string name);
    }
}
