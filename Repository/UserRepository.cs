﻿using CrudOperationUsingDatabase.Context;
using CrudOperationUsingDatabase.Models;
using System.Collections.Generic;
using System.Linq;

namespace CrudOperationUsingDatabase.Repository
{
    public class UserRepository : IUserRepository
    {
        // Call Context Layer of UserDbContext
        // Don't Create a new Keyword
        // Call Constructor and use Constructor DI
        readonly UserDbContext userDbContext;
        public UserRepository(UserDbContext _userDbContext) 
        {
            userDbContext = _userDbContext;
        }

        public void AddUser(User user)
        {
            userDbContext.Users.Add(user);
            userDbContext.SaveChanges();
        }

        public List<User> GetAllUsers()
        {
            return userDbContext.Users.ToList();
        }

        public User GetUserByName(string name)
        {
            return userDbContext.Users.Where(u => u.Name == name).FirstOrDefault(); // Linq option
        }
    }
}
